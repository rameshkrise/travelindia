export function logIn(agentName) {
    return {
        type: "AGENT_NAME",
        agentName
    }
}

export function password(agentPassword){
    return {
        type: "AGENT_PASSWORD",
        agentPassword
    }
}

export function changeCredential(credentialType) {
    return {
        type: "CHANGE_CREDENTIALS",
        credentialType
    }
}

export function tryLogIn(){
    return {
        type: "TRY_LOGIN"       
    }
}

function receivePackages(json) {
    console.log("RECEIVE_PACKAGES "+json);
    return {
        type: "RECEIVE_PACKAGES",
        packages: json.map(child => {
            console.log(child.tour_title);
            return child})
    }
}

export function fetchTourPackages(webServiceToken) {
    console.log("fetchTourPackages ");
    return fetch('http://139.59.44.185/api/v1/tours',{
       method: 'GET',
       headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json',
           'Authorization': 'Basic '.concat(webServiceToken),
       },
    }).then((response) => response.json())
    .then((responseData) => {
        return receivePackages(responseData);
    })
    .catch(function (err) {   
        console.log("fetchTourPackages error", e);             
        return null;
    });
}

//Action to set the fetching status as loading, loaded or failed.
export function fetchTourPackagesStatus(status){
    console.log("fetchTourPackagesStatus");
    return {
        type: "RECEIVE_PACKAGES_STATUS",
        status: status
    }
}

export function fetchTravellersDetails(tourPackage, webServiceToken) {
    console.log("fetchTravellersDetails - tourPackage: "+ tourPackage);
    return fetch('http://139.59.44.185/api/v1/bookings/'.concat(tourPackage),{
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': 'Basic '.concat(webServiceToken),
        }
    }).then((response) => response.json())
    .then((responseData) => {
        return {
            type: "TRAVELLER_DETAILS",
            travellers: responseData
        }
    }).catch(e => {
        console.log("fetchTravellersDetails error", e);
        return null;
    });
}

export function selectedTravellerDetails(travellerDetails) {
    return {
        type: "SELECTED_TRAVELLER_DETAILS",
        details: travellerDetails
    }
}

export function submittedTravellerIdsAction(travellerIds) {
    return {
        type: "SUBMITTED_TRAVELLER_ID_COLLECTION",
        ids: travellerIds
    }
}

export function travellerFeedbackAction(feedback) {
    return {
        type: "TRAVELLER_FEEDBACK",
        feedback: feedback
    }
}

export function webServiceTokenAction(webServiceToken) {
    return {
        type: "WEBSERVICE_TOKEN",
        webServiceToken: webServiceToken
    }
}