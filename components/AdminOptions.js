import React, { Component, PropTypes } from 'react';

import {
    StyleSheet,
    View,
    ListView,
    Text,
    TouchableHighlight,
    ToastAndroid,
    Alert
} from 'react-native';
import NavigateFooter from './NavigateFooter';
import { Actions } from 'react-native-router-flux';

export default class AdminOptions extends Component{
    constructor() {
        super();
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows(['Get tour package details', 'Clear local data', 'Change user credentials']),
        };
    }

    onRowPress(rowData, rowId){
        rowData.isSelect = !rowData.isSelect;
        console.log('Row press', rowData);
        switch (parseInt(rowData)){
            case 0:
                console.log("trying to open package screen");
                Actions.tourPackageScreen();
                break;
            case 1:
                Alert.alert(
                    'Warning',
                    'Currently stored user feedback and coordinator feedback data will be deleted\
            \n\nPress OK to select.',
                    [
                        {
                            text: 'OK', onPress: () => { 
                                global.storage.remove({
                                    key: 'userFeedback'
                                }); 
                                global.storage.save({
                                    key: 'userFeedbackPushed',
                                    data: false
                                });
                                global.storage.save({
                                    key: 'tCFeedbackPushed',
                                    data: false
                                }); 
                                //Need to remove tour coordinator feedback data.  
                                global.storage.remove({
                                    key: 'tCFInputsFirstPart'
                                });
                                global.storage.remove({
                                    key: 'tCFInputsSecondPart'
                                });
                                global.storage.remove({
                                    key: 'tCFInputsEnroute'
                                });
                                global.storage.remove({
                                    key: 'tCFInputsAbst'
                                });
                                global.storage.remove({
                                    key: 'submittedTravellerIds'
                                }); 
                                global.storage.remove({
                                    key: 'selectedPackage'
                                });
                                ToastAndroid.showWithGravity('Local data got cleared!', ToastAndroid.SHORT, ToastAndroid.CENTER);                                 
                            }
                        },
                        {
                            text: 'Cancel', onPress: () => {

                            }
                        }
                    ]
                );                          
                break;
            case 2: 
                //ToastAndroid.showWithGravity('Screen will be opened to change the credentials', ToastAndroid.SHORT, ToastAndroid.CENTER);
                Actions.changeCredentialsSelection();
                break;
            default:
                break;
        }
    }

    onLogoutPress() {
        Actions.loginScreen();
    }

    render(){
        return (
            <View style={{ backgroundColor: 'black', flex: 1}} >
                <ListView style={{ flex:1}}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData, sectionId, rowId)=>{
                        return(
                            <TouchableHighlight onPress={this.onRowPress.bind(rowData, rowId)}>
                                <View style={styles.container}>
                                    <Text style={styles.text}>{rowData}</Text>
                                </View>
                            </TouchableHighlight>
                        )
                    }}
                />                
                <NavigateFooter onPress={this.onLogoutPress} navigationText='LOGOUT' />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        margin: 20,
        marginLeft: 40,
        marginRight:40,
        backgroundColor: '#4286f4',
        alignItems: 'center',
        borderRadius: 10,
        alignSelf:'stretch'
    },
    text: {
        color: 'white',
        fontSize: 16
    }
})