import React, { Component, PropTypes } from 'react';
import Dimensions from 'Dimensions';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Animated,
    Easing,
    Image,
    Alert,
    View,
    ToastAndroid
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';

import spinner from '../images/loading.gif';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        loggedIn: state.login.loggedIn,
        agentName: state.login.agentName,
        agentPassword: state.login.agentPassword,
        webServiceToken: state.login.webServiceToken,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

const CredentialStoreKeys = {
    ADMIN: 'adminCredentials',
    TOURCOORDINATOR: 'tourCoordinatorCredentials',
    WEBSERVICE: 'webServiceCredentials'
};

class ButtonSubmit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        };

        this.buttonAnimated = new Animated.Value(0);
        this.growAnimated = new Animated.Value(0);
        this._onPress = this._onPress.bind(this);
    }

    _onPress() {
        console.log("On press");        
        this.props.fetchTourPackages(this.props.webServiceToken);
        global.storage.load({
            key: CredentialStoreKeys.ADMIN,
            autoSync: true,
            syncInBackground: true,
        }).then(adminCredential => {
            global.storage.load({
                key: CredentialStoreKeys.TOURCOORDINATOR,
                autoSync: true,
                syncInBackground: true,
            }).then(tcCredential => {
                if (this.props.agentName === adminCredential.userName && this.props.agentPassword === adminCredential.password) {
                    Actions.adminOptions();
                } else if (this.props.agentName === tcCredential.userName && this.props.agentPassword === tcCredential.password) {
                    Actions.selectFeedbackForm();
                } else {
                    ToastAndroid.showWithGravity('Invalid user name and/or password!', ToastAndroid.LONG, ToastAndroid.CENTER);                                 
                }
            }).catch(e => {
                // if (this.props.agentName === 'tc' || this.props.agentName === 'Tc') {
                //     Actions.selectFeedbackForm();
                // }
            });
        }).catch(e => {
            ToastAndroid.showWithGravity('Please change Admin user name and password!', ToastAndroid.LONG, ToastAndroid.CENTER);                                             
            if (this.props.agentName === 'admin' || this.props.agentName === 'Admin') {
                Actions.adminOptions();
            } 
        }); 
    }

    _onGrow() {
        Animated.timing(
            this.growAnimated,
            {
                toValue: 1,
                duration: 20,
                easing: Easing.linear
            }
        ).start();
    }

    render() {
        const changeWidth = this.buttonAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [DEVICE_WIDTH - MARGIN, MARGIN]
        });
        const changeScale = this.growAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [1, MARGIN]
        });
        return (
            <View >
                <Animated.View style={{ width: changeWidth }}>
                    <TouchableOpacity style={styles.button}
                        onPress={this._onPress}
                        activeOpacity={1} >
                        {this.state.isLoading ?
                            <Image source={spinner} style={styles.image} />
                            :
                            <Text style={styles.text}>LOGIN</Text>
                        }
                    </TouchableOpacity>
                    <Animated.View style={[styles.circle, { transform: [{ scale: changeScale }] }]} />
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: -95,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4286f4',
        height: MARGIN,
        borderRadius: 10,
        zIndex: 100,
    },
    circle: {
        height: MARGIN,
        width: MARGIN,
        marginTop: -MARGIN,
        borderWidth: 1,
        borderColor: '#F035E0',
        borderRadius: 100,
        alignSelf: 'center',
        zIndex: 99,
        backgroundColor: '#F035E0',
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    image: {
        width: 24,
        height: 24,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ButtonSubmit);