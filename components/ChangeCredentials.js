import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ToastAndroid,
    StyleSheet,
    TouchableHighlight
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import UserInput from './UserInput';
import usernameImg from '../images/username.png';
import passwordImg from '../images/password.png';
import eyeImg from '../images/eye_black.png';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

var Buffer = require('buffer/').Buffer

function mapStateToProps(state) {
    return {
        credentialType:state.login.credentialType
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

const CredentialTypes = {
    ADMIN: 'Admin',
    TOURCOORDINATOR: 'TourCoordinator',
    WEBSERVICE: 'WebService'
};

const CredentialStoreKeys = {
    ADMIN: 'adminCredentials',
    TOURCOORDINATOR: 'tourCoordinatorCredentials',
    WEBSERVICE: 'webServiceCredentials'
};

class ChangeCredentials extends Component {    
    constructor(props){
        super(props);
        this.state = {
            userName: null,
            password: null,
            confirmPassword: null,
            showPass: true,
            press: false           
        };
        this.enterUserName = this.enterUserName.bind(this);
        this.enterPassword = this.enterPassword.bind(this);
        this.enterConfirmPassword = this.enterConfirmPassword.bind(this);
        this.showPass = this.showPass.bind(this);
        this.onOkPress = this.onOkPress.bind(this);
        this.onCancelPress = this.onCancelPress.bind(this);
    }

    enterUserName(userName){
        this.setState({
            userName: userName
        });
    }

    enterPassword(password) {
        this.setState({
            password: password
        });
    }

    enterConfirmPassword(confirmPassword){
        this.setState({
            confirmPassword: confirmPassword
        });
    }

    showPass() {
        this.state.press === false ? this.setState({ showPass: false, press: true }) : this.setState({ showPass: true, press: false });
    }

    onOkPress() {
        var key = "";
        //Check whether both password and confirm passwords are matching
        if (!this.state.password || (this.state.password !== this.state.confirmPassword)){
            ToastAndroid.showWithGravity('Please provide matching password!', ToastAndroid.LONG, ToastAndroid.CENTER);   
            return;                              
        }
        switch (this.props.credentialType) {
            case CredentialTypes.ADMIN:
                key = CredentialStoreKeys.ADMIN;
                break;
            case CredentialTypes.TOURCOORDINATOR:
                key = CredentialStoreKeys.TOURCOORDINATOR;
                break;
            case CredentialTypes.WEBSERVICE:
                key = CredentialStoreKeys.WEBSERVICE;
                const webServiceToken = new Buffer(`${this.state.userName}:${this.state.password}`).toString('base64');
                console.log("webserviceToken generated", webServiceToken);
                global.storage.save({
                    key: 'webServiceToken',
                    data: webServiceToken
                });  
                break;
            default:
                break;
        }
        global.storage.save({
            key: key,
            data: {
                userName: this.state.userName,
                password: this.state.password
            }
        });
        ToastAndroid.showWithGravity('Password changed successfully!', ToastAndroid.LONG, ToastAndroid.CENTER);                                 
        Actions.changeCredentialsSelection();
    }

    onCancelPress() {
        Actions.changeCredentialsSelection();
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableHighlight style={styles.headerContainer}>
                    <Text style={styles.assignText}>
                        Change Credential for {this.props.credentialType}
                    </Text>
                </TouchableHighlight>
                <UserInput source={usernameImg}
                    placeholder='User Name'
                    autoCapitalize={'none'}
                    returnKeyType={'done'}
                    autoCorrect={false}
                    onChangeText={this.enterUserName} />
                <UserInput source={passwordImg}
                    secureTextEntry={this.state.showPass}
                    placeholder='Password'
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    onChangeText={this.enterPassword} />
                <UserInput source={passwordImg}
                    secureTextEntry={this.state.showPass}
                    placeholder='Confirm Password'
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    onChangeText={this.enterConfirmPassword} />
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.btnEye}
                    onPress={this.showPass}
                >
                </TouchableOpacity>
                <View style = {{flex:0.2, alignItems:'center'}}>
                    <View style={styles.submitContainer} >
                        <TouchableHighlight style={styles.bottomContainer} onPress={this.onOkPress}>
                            <Text style={styles.assignText}>
                                OK
                            </Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.bottomContainer} onPress={this.onCancelPress}>
                            <Text style={styles.assignText}>
                                CANCEL
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "black",
    },
    btnEye: {
        position: 'absolute',
        top: 55,
        right: 28,
    },
    iconEye: {
        width: 25,
        height: 25,
        tintColor: 'rgba(0,0,0,0.2)',
    },
    headerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4286f4',
        height: 40,
        top: 10,
        marginBottom: 20,
    },
    bottomContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4286f4',
        height: 40,
        top: 10,
        flex: .5,
        marginHorizontal: 5,
        marginBottom: 20,
    },
    assignText: {
        color: 'white',
        backgroundColor: 'transparent',
        fontSize: 16,
        margin:20,
        paddingLeft: 20,
        textAlign: 'center'
    },
    submitContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',
        margin: 20,
        marginHorizontal: 10
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeCredentials);