import React, { Component, PropTypes } from 'react';

import {
    StyleSheet,
    View,
    ListView,
    Text,
    TouchableHighlight,
    ToastAndroid,
    Alert
} from 'react-native';
import NavigateFooter from './NavigateFooter';
import { Actions } from 'react-native-router-flux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        credentialType: state.login.credentialType
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

const CredentialTypes = {
    ADMIN: 'Admin',
    TOURCOORDINATOR: 'TourCoordinator',
    WEBSERVICE: 'WebService'
}

class ChangeCredentialsSelection extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows([CredentialTypes.ADMIN, CredentialTypes.TOURCOORDINATOR, CredentialTypes.WEBSERVICE]),
        };
    }

    onRowPress(rowData, rowId) {
        rowData.isSelect = !rowData.isSelect;
        console.log('Row press', rowData);
        this.props.changeCredential(rowData);
        Actions.changeCredentials();        
    }

    onHomePress() {
        Actions.adminOptions();
    }

    render() {
        return (
            <View style={{ backgroundColor: 'black', flex: 1 }} >
                <ListView style={{ flex: 1 }}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData, sectionId, rowId) => {
                        return (
                            <TouchableHighlight onPress={this.onRowPress.bind(this,rowData, rowId)}>
                                <View style={styles.container}>
                                    <Text style={styles.text}>{rowData}</Text>
                                </View>
                            </TouchableHighlight>
                        )
                    }}
                />
                <NavigateFooter onPress={this.onHomePress} navigationText='HOME' />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        margin: 20,
        marginLeft: 40,
        marginRight: 40,
        backgroundColor: '#4286f4',
        alignItems: 'center',
        borderRadius: 10,
        alignSelf: 'stretch'
    },
    text: {
        color: 'white',
        fontSize: 16
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangeCredentialsSelection);
