import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableHighlight,
    TextInput,
    Alert,
    ToastAndroid
} from 'react-native';
import UserInput from './UserInput';
import FeedbackRadio from './FeedbackRadio';


import { Actions } from 'react-native-router-flux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

var _ = require('lodash');

function mapStateToProps(state) {
    return {
        travellerFeedback: state.tourpackage.travellerFeedback,
        submittedTravellerIdCollection: state.tourpackage.submittedTravellerIdCollection,
        selectedTraveller: state.tourpackage.selectedTraveller,
        selectedTravellerDetails: state.tourpackage.selectedTraveller
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

const InputType = {
    YES_NO: 'YES_NO',
    FEEDBACK_CHOICES: 'FEEDBACK_CHOICES',
    TEXT_INPUT: 'TEXT_INPUT',
    SOURCE_OF_KNOWLEDGE: 'SOURCE_OF_KNOWLEDGE'
}

class FeebackForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            questions: [
                { i: "1", q: "Is this first time to book through Trave Times (India) P. Ltd? ", type: InputType.YES_NO, updateField: "field_tvl_feedback_firsttime"},
                { i: "2", q: "How did you know about Trave Times (India) P.Ltd?", type: InputType.SOURCE_OF_KNOWLEDGE, updateField: "field_tvl_feedback_how_know"},
                { i: "3", q: "How was your reservation handled, professionally?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_comfort" },
                { i: "4", q: "Accommodation Characteristics\n\na. Cleanliness and Hygiene?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_hygiene" },
                { i: "b", q: "Housekeeping Service?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_housekeeping" },
                { i: "c", q: "Friendliness of Staff? ", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_staf_friendly"},
                { i: "d", q: "Competence of Staff? ", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_compt_staff"},
                { i: "e", q: "Quality of the service provided? ", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_quality"},
                { i: "f", q: "Hotel/Villa/Resort/House Boat/Tree House Ambience?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_ambience"},
                { i: "g", q: "Hotel/Villa/Resort/House Boat/Tree House Location? ", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_location"},
                { i: "5", q: "Transportation Characteristics\n\na. Quality of the vehicle?", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_vehicle_qual"},
                { i: "b", q: "Service and support of the transport staff?", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_trans_sta"},
                { i: "c", q: "Comforatbleness of your overall journey?", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_comfort"},
                { i: "6", q: "Quality of the Meals and Menu provided?", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_meals_qual"},
                { i: "7", q: "Service of the Tour Escort", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_service_escrt"},
                { i: "8", q: "General Overview\n\na. How do you rate your overall stay?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_rate_stay" },
                { i: "b", q: "How do you rate your overall journey?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_rate_jny"},
                { i: "c", q: "How do you rate your overall enjoyment of the tour?", type: InputType.FEEDBACK_CHOICES, updateField:  "field_tvl_feedback_overall_enj"},
                { i: "9", q: "Quality versus Price\n\nHow do you rate between Quality/Price?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_quality_price"},
                { i: "10", q: "Travel Times (India) P. Ltd.\n\na. How do you rate Trave Times (India) P. Ltd. Service?", type: InputType.FEEDBACK_CHOICES, updateField: "field_tvl_feedback_travel_times"},
                { i: "b", q: "Would consider our service in the future?", type: InputType.YES_NO, updateField: "field_tvl_feedback_consi_service"},
                { i: "c", q: "Would you recommand us to someone else?", type: InputType.YES_NO, updateField: "field_tvl_feedback_refer_us" },
                { i: "11", q: "Was The Itinerary /Brochure Self Explanatory", type: InputType.YES_NO, updateField: "field_tvl_feedback_itineary_exp" },
                { i: "12", q: "Was Array Of Tours Offered For Selection?", type: InputType.YES_NO, updateField: "field_tvl_feedback_tour_select" },
            ],
            inputs: [
                { i: "Name", field_name: "travelers_name", text:""},
                { i: "Period of Travel", field_name: "", text: ""},
                { i: "Tour Program", field_name: "travelers_code", text: ""},
                { i: "Places Covered", field_name: "", text: "" },
                { i: "Email", field_name: "travelers_email", text: "" },
                { i: "Contact No", field_name: "travelers_mobile_1", text: "" },
                { i: "Nationality", field_name: "travelers_nationality", text: "" },
                { i: "No of Travelers", field_name: "", text: ""}
            ],
            currentTravellerId: -1,
            textInputFeedback: null,
        }
    }

    componentWillMount() {
        console.log("FeedbackForm ComponentWillMount", this.props);
        //Update the inputs state with the received details
        this.state.inputs.map((input, index) => {
            switch (input.field_name) {
                case "travelers_name":
                    input.text = this.props.details.travelers_name;
                    break;
                case "travelers_code":
                    input.text = this.props.details.travelers_code;
                    break;
                case "travelers_email":
                    input.text = this.props.details.travelers_email;
                    break;
                case "travelers_mobile_1":
                    input.text = this.props.details.travelers_mobile_1;
                    break;
                case "travelers_nationality":
                    input.text = this.props.details.travelers_nationality;
                    break;
                default:
                    break;
            }
            return input;
        });

        //Update the current traveller id
        this.setState({
            currentTravellerId: this.props.details.travelers_id
        });
        
        this.props.travellerFeedbackAction(null);
        
    }

    onSumbitPress() {
        //Show the warning and upon confirmation update the feedback.
        Alert.alert(
            'Warning',
            'Once feedback submitted, you cannot edit it again.\
            \n\nPress OK to select.',
            [
                {
                    text: 'OK', onPress: () => {
                        let finalUpdate = [];
                        let userFeedbackObj = {};
                        if(this.updateFeedback()) {
                            userFeedbackObj= {
                                "type": [
                                    {
                                        "target_id": "travellers_feedback",
                                        "target_type": "node_type"
                                    }
                                ],
                                "status": [
                                    {
                                        "value": true
                                    }
                                ],
                                "title": [
                                    {
                                        "value": "Feedback via android"
                                    }
                                ],
                                "field_traveller_name_content": [
                                    {
                                        "target_id": this.state.currentTravellerId
                                    }
                                ]
                            };
                            let temp = this.props.travellerFeedback;
                            temp = temp.reduce(function (result, item) {
                                var key = Object.keys(item)[0];
                                result[key] = item[key];
                                return result;
                            }, {});
                            userFeedbackObj = _.merge(userFeedbackObj, temp);
                            console.log("userFeedbackObj", userFeedbackObj);

                            global.storage.load({
                                key: 'selectedPackageId',
                                autoSync: true,
                                syncInBackground: true,
                            }).then(res => {                           
                                userFeedbackObj = _.merge(userFeedbackObj, {
                                    "field_tour_name_content": [
                                        {
                                            "target_id": res
                                        }
                                    ]
                                });
                                
                                global.storage.load({
                                    key: 'userFeedback',
                                    autoSync: true,
                                    syncInBackground: true,
                                }).then(res => {
                                    console.log("Reterieved from storage: " + JSON.stringify(res));
                                    res.push([userFeedbackObj]);                                
                                    console.log("Before saving: ", res);
                                    global.storage.save({
                                        key: 'userFeedback',
                                        data: res
                                    }).catch(e => {
                                        console.log("Error: ", e);
                                    });
                                }).catch(e => {
                                    //Store this entire data in local storage for the first time.
                                    global.storage.save({
                                        key: 'userFeedback',
                                        data: [[userFeedbackObj]]
                                    });
                                    console.log("Error: ", e);
                                });
                            }).catch(e => {
                                console.log("Error: ", e);
                            });
                            let collection = this.props.submittedTravellerIdCollection || [];
                            collection.push(this.state.currentTravellerId);
                            //Remove the user list from user list screen
                            this.props.submittedTravellerIdsAction(collection);
                            global.storage.save({
                                key: 'userFeedbackPushed',
                                data: false
                            });
                            global.storage.save({
                                key:'submittedTravellerIds',
                                data: collection
                            });
                            Actions.userList();
                        }
                    }
                },
                {
                    text: 'Cancel', onPress: () => {

                    }
                }
            ]
        );
       
    }

    updateFeedback() {
        let temp = this.props.travellerFeedback;
        let foundUpdateIndex = -1;
        let dynKey = {};
        if(temp) {
            for (let i = 0; i < temp.length; i++) {
                if (temp[i]["field_tvl_feedback_choice_dest"]) {
                    console.log("foundUpdateIndex");
                    foundUpdateIndex = i;
                    break;
                }
            }
            if (foundUpdateIndex > -1) {
                temp.splice(foundUpdateIndex, 1);
            }
            dynKey["field_tvl_feedback_choice_dest"] = [{ value: this.state.textInputFeedback || ""}];
            temp.push(dynKey);
            this.props.travellerFeedbackAction(temp);
            return true;
        } else {
            ToastAndroid.showWithGravity('Please provide the feedback before submitting!', ToastAndroid.LONG, ToastAndroid.CENTER);
            return false;
        }
    }

    onTextInputFeedbackSubmit() {
        console.log("FeedbackForm onTextInputFeedbackSubmit");        
        this.updateFeedback();
    }

    onChangeText(text) {
        console.log("FeedbackForm onChangeText", text);
        this.setState({
            textInputFeedback: text
        });
    }

    render(){
        return (
            <View style={styles.container}>                
                <ScrollView style={styles.feedbackcontainer}
                    showsVerticalScrollIndicator={true}>
                    <Text style={styles.heading}>Travel Feedback Form{"\n\n"}</Text>
                    <Text style={styles.intro}>Thank you for choosing us as your trustful travel partner and we hope that you have enjoyed our service.
                        {"\n\n"} We commit to provide a satisfactory service to our entire guest.
                        In order to achieve this aim we need to know your opinion on it.
                        Praise would be a motivation for us to continue our services and any critic would 
                        naturally be a reason for us to improve our services accodring to the requirements 
                        and desires of our guests.{"\n\n\n"}
                    </Text>
                    {this.state.inputs.map((input, index)=>{
                        return(
                            <View key={index}>
                                <UserInput
                                    style={styles.input}
                                    placeholder={input.i}
                                    autoCapitalize={'none'}
                                    returnKeyType={'done'}
                                    autoCorrect={false}
                                    value={input.text} 
                                    key={'ui'+index}/>
                            </View>
                        )
                    })}
                
                
                    {this.state.questions.map((q, i) =>{
                        return (
                            <View key={i}>
                                <Text style={styles.intro} key={'txt'+i}>
                                    {q.i}. {q.q}                      
                                </Text>
                                <FeedbackRadio q={q.i} type={q.type} updateField={q.updateField} key={'fbr'+i}/>
                            </View>
                        )
                    })}  

                    <Text style={styles.intro}>
                    13. Please write your review and comments about the overall tour experience/service.
                    </Text>
                    <TextInput style={styles.textInput} 
                        multiline={true}
                        blurOnSubmit={false}
                        maxLength={1000}
                        onSubmitEditing={this.onTextInputFeedbackSubmit.bind(this)}
                        onChangeText={this.onChangeText.bind(this)}>
                    </TextInput>
                </ScrollView>  
                <View style={styles.submitContainer}>
                    <TouchableHighlight style={styles.headerContainer} onPress={this.onSumbitPress.bind(this)}>
                        <Text style={styles.assignText}>
                            SUBMIT
                        </Text>
                    </TouchableHighlight>
                </View>              
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex: 1,
    },
    feedbackcontainer: {
        flex: 1
    },
    heading: {
        color: 'white',
        textAlign: 'center',
        fontSize: 25
    },
    intro: {
        color: 'white',
        fontSize: 20
    },
    input: {        
        borderColor:'#28aadc',
        display: 'block',
        borderWidth: 20,
    },
    submitContainer: {
        flex: .10,
    },
    assignText: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    textInput: {
        color: 'white',
        backgroundColor: 'transparent',
        borderColor: 'white',
        borderWidth: 2
    },
    headerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4286f4',
        height: 40,
        top: 10,
        zIndex: 100,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(FeebackForm);