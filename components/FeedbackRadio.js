import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    ScrollView,
    Text,
    View
} from 'react-native';

import Button from 'react-native-button';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        travellerFeedback: state.tourpackage.travellerFeedback,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}


const InputType = {
    YES_NO: 'YES_NO',
    FEEDBACK_CHOICES: 'FEEDBACK_CHOICES',
    TEXT_INPUT: 'TEXT_INPUT',
    SOURCE_OF_KNOWLEDGE: 'SOURCE_OF_KNOWLEDGE'
}

class FeedbackRadio extends Component {
    constructor(props) {
        super(props)
        this.state = {            
            feedbackOptions: [{ label: 'Excellent', key: 'excellent', value: 0 }, { label: 'Good', key: 'good', value: 1 }, { label: 'Fair', key:'fair', value: 2 }, { label: 'Poor', key:'poor', value: 3 }],
            confirmationOptions: [{ label: 'Yes',key:'yes', value: 0 }, { label: 'No', key:'no', value: 1 }],
            sourceOptions: [{ label: 'Internet', key:'internet', value: 0 }, { label: 'Advertisement', key:'advertisement', value: 1 }, { label: 'Reference', key:'reference', value: 2 }],
            value: 0,
            valueIndex: -1,
        }
    }

    renderRadioButtons(onPress, obj, i) {
        return (
            <RadioButton labelHorizontal={true} key={i} >
                <RadioButtonInput
                    obj={obj}
                    index={i}
                    isSelected={this.state.valueIndex === i}
                    onPress={onPress}
                    buttonInnerColor='white'
                    buttonOuterColor='white'
                    buttonSize={16}
                    buttonStyle={{}}
                    buttonWrapStyle={{ marginLeft: 15 }}
                />
                <RadioButtonLabel
                    obj={obj}
                    index={i}
                    onPress={onPress}
                    labelStyle={{ fontWeight: 'bold', color: '#4885ed', fontSize: 16 }}
                    labelWrapStyle={{ marginLeft: 5 }}
                />
            </RadioButton>
        )
    }

    renderOptions(options) {               
        return (
            <RadioForm formHorizontal={true}  >
                {options.map((obj, i) => {
                    var onPress = (value, index) => {
                        let key = this.props.updateField;
                        let feedback;
                        let temp;
                        let foundUpdateIndex = -1;
                        let dynKey = {};
                        console.log("Onpress", "Index: "+index + "props: "+ JSON.stringify(this.props));
                        //Stores the appropriate data based on the prop type
                        switch(this.props.type) {
                            case InputType.FEEDBACK_CHOICES:                                
                                feedback = this.state.feedbackOptions[index].key;
                                break;
                            case InputType.SOURCE_OF_KNOWLEDGE:
                                feedback = this.state.sourceOptions[index].key;
                                break;
                            case InputType.YES_NO:
                                feedback = this.state.confirmationOptions[index].key;
                                break;
                            default: 
                                break;                                
                        }

                        temp = this.props.travellerFeedback || [];
                        for (let i=0; i < temp.length; i++) {
                            if(temp[i][key]){
                                console.log("foundUpdateIndex");
                                foundUpdateIndex = i;
                                break;
                            }
                        }

                        if(foundUpdateIndex > -1) {
                            temp.splice(foundUpdateIndex, 1);
                        }
                        
                        dynKey[key] = [{ value: feedback }];                        
                        temp.push(dynKey);
                        
                        this.props.travellerFeedbackAction(temp);                                            
                        
                        this.setState({
                            value: value,
                            valueIndex: index
                        });
                        console.log("TravellerFeedbackProps: ", JSON.stringify(this.props.travellerFeedback));
                    }
                    return this.renderRadioButtons(onPress, obj, i);
                })}
            </RadioForm>
        );
    }

    render() {
        let choice = null;
        if (this.props.type === InputType.FEEDBACK_CHOICES) {
            choice = this.state.feedbackOptions;
        } else if(this.props.type === InputType.YES_NO){
            choice = this.state.confirmationOptions;
        } else {
            choice = this.state.sourceOptions;
        }

        return (
            <View >
                <ScrollView>
                    <View style={styles.component}>
                        {this.renderOptions(choice)}                     
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },   
    component: {
        alignItems: 'center',
        marginBottom: 50,
        marginTop: 10,
    },
    radioStyle: {
        borderRightWidth: 1,
        borderColor: '#2196f3',
        paddingRight: 10
    },
    radioButtonWrap: {
        marginRight: 5
    },
    selectedText: {
        color:'green',
        fontSize: 16
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(FeedbackRadio);