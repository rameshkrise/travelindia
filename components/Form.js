import React, { Component, PropTypes } from 'react';
import Dimensions from 'Dimensions';
import {
    StyleSheet,    
    View,
    ActivityIndicator,
    TouchableOpacity,
    Image,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import UserInput from './UserInput';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';

import usernameImg from '../images/username.png';
import passwordImg from '../images/password.png';
import eyeImg from '../images/eye_black.png';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        loggedIn: state.login.loggedIn,
        agentName: state.login.agentName,
        agentPassword: state.login.agentPassword
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            press: false,
        };
        this.showPass = this.showPass.bind(this);
        this.enterAgentName = this.enterAgentName.bind(this);
        this.enterAgentPassword = this.enterAgentPassword.bind(this);
    }

    componentWillMount(){
        this.props.logIn(null);
        this.props.password(null);   
    }

    showPass() {
        this.state.press === false ? this.setState({ showPass: false, press: true }) : this.setState({ showPass: true, press: false });
    }

    enterAgentName(agentName) {
        this.props.logIn(agentName);
    }

    enterAgentPassword(agentPassword){        
        this.props.password(agentPassword);
    }

    render() {  
        global.storage.load({
            key: 'webServiceToken',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            this.props.webServiceTokenAction(res);
        }).catch(e => {
            this.props.webServiceTokenAction("YWRtaW46YWRtaW4=");
        });     
        return (
            <View style={styles.container}>
                <UserInput source={usernameImg}
                    placeholder='UserName'
                    autoCapitalize={'none'}
                    returnKeyType={'done'}
                    autoCorrect={false}                     
                    onChangeText={this.enterAgentName}/>
                <UserInput source={passwordImg}
                    secureTextEntry={this.state.showPass}
                    placeholder='Password'
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false} 
                    onChangeText={this.enterAgentPassword}/>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.btnEye}
                    onPress={this.showPass}
                >
                    <Image source={eyeImg} style={styles.iconEye} />
                </TouchableOpacity>
            </View>
        );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,        
    },
    btnEye: {
        position: 'absolute',
        top: 55,
        right: 28,
    },
    iconEye: {
        width: 25,
        height: 25,
        tintColor: 'rgba(0,0,0,0.2)',
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);
