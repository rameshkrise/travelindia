import React, { Component, PropTypes } from 'react';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {StyleSheet, View } from 'react-native'

export default class LoginScreen extends Component {
    
    render() {
        return ( 
            <Wallpaper>                          
                <KeyboardAwareScrollView
                    style={{ backgroundColor: "black" }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={styles.container}
                    scrollEnabled={true}>
                    <Logo/>   
                    <View style={styles.bottomContainer}>
                        <Form/>
                        <ButtonSubmit/>  
                    </View>
                </KeyboardAwareScrollView>                        
            </Wallpaper>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',  
    },
    bottomContainer: {
        flex: 0.5,
        alignItems: 'center',
        justifyContent: 'center'
    }
});