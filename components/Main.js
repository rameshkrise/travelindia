import React, { Component } from 'react';
import { Router, Scene, Actions, ActionConst } from 'react-native-router-flux';

import LoginScreen from './LoginScreen';
import TravellerFeedback from './TravellerFeedback';
import TourPackageDetails from './TourPackageDetails';
import AdminOptions from './AdminOptions';
import SelectFeedbackForm from './SelectFeedbackForm';
import UserList from './UserList';
import TourCoordinatorFeedback from './TourCoordinatorFeedback';
import ChangeCredentialsSelection from './ChangeCredentialsSelection';
import ChangeCredentials from './ChangeCredentials';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        loggedIn: state.login.loggedIn
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

class Main extends Component {   
    constructor(props){
        super(props);        
    } 

    render() {      
        return (
            <Router>
                <Scene key="root">                    
                    <Scene key="loginScreen"
                        component={LoginScreen}
                        animation='fade'
                        hideNavBar={true}
                        initial={true}
                        type='reset'                           
                    />
                    <Scene key="tourPackageScreen"
                        component={TourPackageDetails}
                        hideNavBar={true}
                    />
                    <Scene key="adminOptions"
                        component={AdminOptions}   
                        hideNavBar={true}                     
                        type='reset'
                    />
                    <Scene key="selectFeedbackForm"
                        component={SelectFeedbackForm}
                        hideNavBar={true}
                        type='reset'
                    />
                    <Scene key="travellerFeedback"
                        component={TravellerFeedback}
                        animation='fade'
                        hideNavBar={true}
                    />
                    <Scene key="userList"
                        component={UserList}
                        animation='fade'
                        hideNavBar={true}
                        type='reset'
                    />
                    <Scene key="tourCoordinatorFeedback"
                        component={TourCoordinatorFeedback}
                        animation='fade'
                        hideNavBar={true}
                    />
                    <Scene key="changeCredentials"
                        component={ChangeCredentials}
                        type='reset'
                        animation='fade'
                        hideNavBar={true}
                    />
                    <Scene key="changeCredentialsSelection"
                        component={ChangeCredentialsSelection}
                        animation='fade'
                        type='reset'
                        hideNavBar={true}
                    />
                </Scene>
            </Router>
        );
    }
}
 
const APP = connect(mapStateToProps, mapDispatchToProps)(Main);

export default APP;