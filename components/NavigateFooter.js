import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableHighlight,
} from 'react-native';

export default class NavigateFooter extends Component {
    render() {
        return (
            <View>
                <TouchableHighlight onPress={this.props.onPress.bind(this)} style={styles.container}>
                    <Text style={{ color: 'white' }}>
                        {this.props.navigationText}
                    </Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        paddingRight:20,
        paddingLeft:20,
        margin: 10,
        backgroundColor: '#4286f4',
        alignItems: 'center',
        alignSelf: 'center',
        zIndex: 200
    },
    text: {
        color: '#4f603c'
    }
});