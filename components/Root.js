import React, { Component } from 'react'
import APP from './Main';
import { Provider } from 'react-redux';
import store from '../store/configureStore';
import { View, Text, AsyncStorage } from 'react-native';

import Storage from 'react-native-storage';

export default class RootComponent extends Component {
    
    render() {   
        let storage = new Storage({
            size: 10000,
            storageBackend: AsyncStorage,
            defaultExpires: null,
            enableCache: true,
            sync: {

            } 
        });   
        global.storage = storage;
        return (
            <Provider store={store}>           
                <APP />               
            </Provider>
            
        );
    }
}

