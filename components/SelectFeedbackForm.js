import React, { Component, PropTypes } from 'react';

import {
    StyleSheet,
    View,
    ListView,
    Text,
    TouchableHighlight,
    ToastAndroid
} from 'react-native';
import NavigateFooter from './NavigateFooter';
import { Actions } from 'react-native-router-flux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
var _ = require('lodash');
function mapStateToProps(state) {
    return {
        webServiceToken: state.login.webServiceToken,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

class SelectFeedbackForm extends Component {
    constructor() {
        super();
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows(['Travelers Feedback Form', 'Travel Coordinator Feedback Form', 'Push User Feedback Data',
            'Push Travel Coordinator Feedback Data']),
            tCFeedbackPushed: false,
            userFeedbackPushed: false
        };
    }

    componentWillMount(){
        global.storage.load({
            key: 'userFeedbackPushed',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            this.setState({
                userFeedbackPushed: res
            });
        });
        global.storage.load({
            key: 'tCFeedbackPushed',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            this.setState({
                tCFeedbackPushed: res
            });
        });
    }

    //Input is expected in the format of [{i:"", field_name:"", text:""}]
    formatTCFObject(input) {
        let resultObj = {};
        let dynKey = [];
        let key = "";

        for(let i=0; i<input.length; i++){
            key = input[i].field_name;            
            if(key !== null){
                dynKey[key] = [{
                    value: input[i].text
                }];
                resultObj = _.merge(resultObj, dynKey);
            }
        }

        console.log("formatTCFObject", resultObj);
        return resultObj;
    }

    pushTCFToServer(input) {
        global.storage.load({
            key: 'tCFeedbackPushed',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            if(res) {
                ToastAndroid.showWithGravity('Already tour coordinator feedback data pushed to server.', ToastAndroid.LONG, ToastAndroid.CENTER);                                            
            } else {
                fetch('http://139.59.44.185/entity/node', {
                    method: "POST",
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                        'Authorization': 'Basic '.concat(this.props.webServiceToken),
                    },
                    body: JSON.stringify(input)
                }).then((response) => response.json())
                    .then((responseData) => {
                        console.log("TCF Responded data: " + JSON.stringify(responseData));
                        global.storage.save({
                            key: 'tCFeedbackPushed',
                            data: true
                        });
                        //Also update the state
                        this.setState({
                            tCFeedbackPushed: true
                        });
                        ToastAndroid.showWithGravity('Pushed tour coordinator feedback data to server.', ToastAndroid.LONG, ToastAndroid.CENTER);                            
                });
            }
        });
    }

    pushUserFeedback(){
        global.storage.load({
            key: 'userFeedback',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            let i = 0;
            console.log("res: ", res);
            let fetchCall = function () {
                console.log("Post data: ", JSON.stringify(res[i][0]));
                fetch('http://139.59.44.185/entity/node', {
                    method: "POST",
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                        'Authorization': 'Basic '.concat(this.props.webServiceToken),
                    },
                    body: JSON.stringify(res[i][0])
                }).then((response) => response.json())
                .then((responseData) => {
                    console.log("Responded data: " + JSON.stringify(responseData));
                    i++;
                    if (i < res.length) {
                        fetchCall();
                    } else {
                        //Better to update the status of push to true and reset it whenever user submit fresh feedback. TODO::                                
                        global.storage.save({
                            key: 'userFeedbackPushed',
                            data: true
                        });                            
                        global.storage.remove({
                            key: 'userFeedback'
                        });                            
                        this.setState({
                            userFeedbackPushed: true
                        });
                        ToastAndroid.showWithGravity('Pushed user feedback data to server.', ToastAndroid.LONG, ToastAndroid.CENTER);
                    }
                }).catch(e => {
                    console.log("Error: ", e);
                });
            }.bind(this);
            fetchCall();
        }).catch(e => {
            console.log("Error: ", e);
            ToastAndroid.showWithGravity('Failed to push.' + e, ToastAndroid.LONG, ToastAndroid.CENTER);
            return;
        });    
       
    }

    onRowPress(rowData, rowId) {
        rowData.isSelect = !rowData.isSelect;
        switch (parseInt(rowId)) {
            case 0:
                Actions.userList();
                break;
            case 1:
                //ToastAndroid.showWithGravity('Travel Coordinator Feedback Form will be opened!', ToastAndroid.SHORT, ToastAndroid.CENTER);                
                global.storage.load({
                    key: 'tCFSubmitted',
                    autoSync: true,
                    syncInBackground: true,
                }).then(res => {
                    console.log("Reterieved from storage: " + JSON.stringify(res)); 
                    if(res) {
                        ToastAndroid.showWithGravity('Travel Coordinator Feedback Form is been submitted already!', ToastAndroid.LONG, ToastAndroid.CENTER);
                        //TODO:: Testing purpose. Needs to be removed on deployment.
                        //Actions.tourCoordinatorFeedback();                        
                    } else {
                        Actions.tourCoordinatorFeedback();
                    }
                }).catch(e => {                   
                    console.log("Error: ", e);
                    //This is required for the first time. TODO:: onNotFoundError only open it. 
                    Actions.tourCoordinatorFeedback();
                });                
                break;
            case 2:
                global.storage.load({
                    key: 'userFeedbackPushed',
                    autoSync: true,
                    syncInBackground: true,
                }).then(res => {
                    if(res) {
                        ToastAndroid.showWithGravity('Already user feedback data pushed to server.', ToastAndroid.LONG, ToastAndroid.CENTER);
                    } else {
                        this.pushUserFeedback();                        
                    }
                }).catch(e => {
                    //For the first time
                    this.pushUserFeedback();
                });
                break;
            case 3:
                //Pushing tourcoordinator feedback data.
                let finalUpdate = [];
                let tCFeedbackObj = {
                    "type": [
                        {
                            "target_id": "tour_coordinator_feedback",
                            "target_type": "node_type"
                        }
                    ],
                    "status": [
                        {
                            "value": true
                        }
                    ],
                    "title": [
                        {
                            "value": "Feedback via android"
                        }
                    ]
                };
                global.storage.load({
                    key: 'selectedPackageId',
                    autoSync: true,
                    syncInBackground: true,
                }).then(res => {
                    tCFeedbackObj = _.merge(tCFeedbackObj, {
                        "field_tour_name_content": [
                            {
                                "target_id": res
                            }
                        ]
                    });
                    global.storage.load({
                        key: 'tCFInputsFirstPart',
                        autoSync: true,
                        syncInBackground: true,
                    }).then(res => {
                        console.log("tCFInputsFirstPart", res);
                        //construct the object                                                                 
                        tCFeedbackObj = _.merge(tCFeedbackObj, this.formatTCFObject(res));
                        console.log("after tCFInputsFirstPart: " + JSON.stringify(tCFeedbackObj));
                        global.storage.load({
                            key: 'tCFInputsSecondPart',
                            autoSync: true,
                            syncInBackground: true,
                        }).then(res => {
                            tCFeedbackObj = _.merge(tCFeedbackObj, this.formatTCFObject(res));
                            console.log("after tCFInputsSecondPart: " + JSON.stringify(tCFeedbackObj));
                            global.storage.load({
                                key: 'tCFInputsEnroute',
                                autoSync: true,
                                syncInBackground: true,
                            }).then(res => {
                                tCFeedbackObj = _.merge(tCFeedbackObj, this.formatTCFObject(res));
                                console.log("after tCFInputsEnroute: " + JSON.stringify(tCFeedbackObj));
                                global.storage.load({
                                    key: 'tCFInputsAbst',
                                    autoSync: true,
                                    syncInBackground: true,
                                }).then(res => {
                                    tCFeedbackObj = _.merge(tCFeedbackObj, this.formatTCFObject(res));
                                    console.log("after tCFInputsAbst: " + JSON.stringify(tCFeedbackObj));

                                    //Push the final data to server
                                    this.pushTCFToServer(tCFeedbackObj);
                                });
                            });
                        });
                    });
                }).catch(e => {
                    console.log("Error: ", e);
                    ToastAndroid.showWithGravity('Failed to push!', ToastAndroid.LONG, ToastAndroid.CENTER);                
                });  
                break;
            default:
                break;
        }
    }

    onLogoutPress() {
        Actions.loginScreen();
    }

    render() {
        return (
            <View style={{ backgroundColor: 'black', flex: 1 }}>
                <ListView style={{ flex: 1 }}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData, sectionId, rowId) => {
                        return (
                            <TouchableHighlight onPress={this.onRowPress.bind(this, rowData, rowId)}>
                                <View style={styles.container}>
                                    <Text style={styles.text}>{rowData}</Text>
                                </View>
                            </TouchableHighlight>
                        )
                    }}
                />
                <Text style={styles.text}>
                    User Feedback Pushed?{this.state.userFeedbackPushed?"\t\tYes\n": "\t\tNo\n"}
                </Text>                
                <Text style={styles.text}>                
                    Tour Coordinator Feedback Pushed?{this.state.tCFeedbackPushed ? "\t\tYes\n" : "\t\tNo\n"}
                </Text>                
                <NavigateFooter onPress={this.onLogoutPress} navigationText='LOGOUT' />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        margin: 10,
        backgroundColor: '#4286f4',
        alignItems: 'center',
        borderRadius: 10,
    },
    text: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectFeedbackForm);