import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    Alert,
    ScrollView,
    Dimensions
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions, ActionConst } from 'react-native-router-flux';
import UserInput from './UserInput';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';


let DEVICE_WIDTH;

function mapStateToProps(state) {
    return {
        //travellerDetails: state.tourpackage.travellerDetails ,
        selectedTraveller: state.tourpackage.selectedTraveller
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

class TourCoordinatorFeedback extends Component{
    constructor(props) {
        super(props);
        this.state = {
            inputs_first_part: [
                // { i: "Name", field_name: null, text: "" },
                { i: "Tour Manager's Boarding Point", field_name: "field_tour_fdbk_boarding_pt", text: "" },
                { i: "Coach", field_name: "field_tour_fdbk_coach", text: "" },
                { i: "Coach No", field_name: "field_tour_fdbk_coach_no", text: "" },
                //{ i: "Tour Code", field_name: "", text: "" },
                { i: "Departure From", field_name: "field_tour_fdbk_depart_from", text: "" },
                { i: "To", field_name: "field_tour_fdbk_depart_to", text: "" },
                { i: "Circuit", field_name: "field_tour_fdbk_circuit", text: "" },
                { i: "Starting Point", field_name: "field_tour_fdbk_starting_pt", text: "" },               
            ],
            inputs_second_part: [
                { i: "Enroute Bording", field_name: null, text: "" },
                { i: "Absentees", field_name: null, text: "" },
                { i: "Coach Position: From Engine", field_name: "field_tour_fdbk_coachpst_engine", text: "" },
                { i: "Coach Position: From Pantry", field_name: "field_tour_fdbk_coachpst_pantry", text: "" },
                { i: "Coach Supplier Names", field_name: "field_tour_fdbk_suppliers_name", text: "" },
                { i: "Coach Security Name", field_name: "field_tour_fdbk_coach_security", text: "" },
                { i: "Coach Security Mobile Number", field_name: null, text: "" },
                { i: "Travel Season", field_name: "field_tour_fdbk_travel_season", text: "" },
                { i: "Quality of Itinerary", field_name: "field_tour_fdbk_quality_itn", text: "" },
                { i: "Hotel Service", field_name: "field_tour_fdbk_hotel_service", text: "" },
                { i: "Transport Service", field_name: "field_tour_fdbk_trans_service", text: "" },
                { i: "Customer Awareness Of The Destination", field_name: "field_tour_fdbk_cust_aware", text: "" },
                { i: "Tour Suggestions - Any Options Requeired", field_name: "field_tour_fdbk_suggestions", text: "" },
                { i: "Problems Faced During The Tour - If Any", field_name: "field_tour_fdbk_problems", text: "" },
                { i: "Hotel Room Request", field_name: "field_tour_fdbk_hotel_service", text: "" },
                { i: "No of Tourists", field_name: "field_tour_fdbk_hotel_no_tourist", text: "" },
                { i: "Double", field_name: "field_tour_fdbk_hotel_rm_double", text: "" },
                { i: "Tripple", field_name: "field_tour_fdbk_hotel_tripple", text: "" },
                { i: "Name of place and Hotel", field_name: "field_tour_fdbk_hotel_accomod", text: "" },
                { i: "Accomodation place and name", field_name: "field_tour_fdbk_hotel_location", text: "" },
                { i: "Remarks on coach, food, sick patients, deboarding patients, cleanliness of the coach", field_name: "field_tour_fdbk_remarks", text: "" },
            ],
            inputs_enroute: [
                { i: "MDU", field_name: "field_tour_fdbk_enroute_mdu", text: ""},
                { i: "DG", field_name: "field_tour_fdbk_enroute_dg", text: "" },
                { i: "KRR", field_name: "field_tour_fdbk_enroute_krr", text: "" },
                { i: "ED", field_name: "field_tour_fdbk_enroute_ed", text: "" },
                { i: "SA", field_name: "field_tour_fdbk_enroute_sa", text: "" },
                { i: "JRT", field_name: "field_tour_fdbk_enroute_jrt", text: "" },
                { i: "MAS", field_name: "field_tour_fdbk_enroute_mas", text: "" },
                { i: "TOTAL", field_name: "field_tour_fdbk_enroute_total", text: "" },
            ],
            inputs_abst: [
                { i: "MDU", field_name: "field_tour_fdbk_abst_mdu", text: "" },
                { i: "DG", field_name: "field_tour_fdbk_abst_dg", text: "" },
                { i: "KRR", field_name: "field_tour_fdbk_abst_krr", text: "" },
                { i: "ED", field_name: "field_tour_fdbk_abst_ed", text: "" },
                { i: "SA", field_name: "field_tour_fdbk_abst_sa", text: "" },
                { i: "JRT", field_name: "field_tour_fdbk_abst_jrt", text: "" },
                { i: "MAS", field_name: "field_tour_fdbk_abst_mas", text: "" },
                { i: "TOTAL", field_name: "field_tour_fdbk_abst_total", text: "" },
            ],
            // deviceWidth: 80,
            // deviceHeight: 0,

        }
    }

    onEnrouteEntered(index, text) {
        const newArr = [...this.state.inputs_enroute];
        newArr[index].text = text;
        this.setState({
            inputs_enroute: newArr
        });
    }
    
    onAbstEntered(index, text) {
        const newArr = [...this.state.inputs_abst];
        newArr[index].text = text;
        this.setState({
            inputs_abst: newArr
        });
    }

    onInputFirstPartChanged(index, text) {
        const newArr = [...this.state.inputs_first_part];
        newArr[index].text = text;
        this.setState({
            inputs_first_part: newArr
        });        
    }

    onInputSecondPartChanged(index, text) {
        const newArr = [...this.state.inputs_second_part];
        newArr[index].text = text;
        this.setState({
            inputs_second_part: newArr
        });
    }

    //Save all the value in the local storage.
    onSavePress() {
        global.storage.save({
            key: 'tCFInputsFirstPart',
            data: this.state.inputs_first_part
        }).catch(e => {
            console.log("Error: ", e);
        });
        global.storage.save({
            key: 'tCFInputsSecondPart',
            data: this.state.inputs_second_part
        }).catch(e => {
            console.log("Error: ", e);
        });
        global.storage.save({
            key: 'tCFInputsEnroute',
            data: this.state.inputs_enroute
        }).catch(e => {
            console.log("Error: ", e);
        });
        global.storage.save({
            key: 'tCFInputsAbst',
            data: this.state.inputs_abst
        }).catch(e => {
            console.log("Error: ", e);
        });

    }

    /**
     * Store the status of the submit. This will be reset in AdminOptions while choosing the tour package.
     * Also, once tour coordinator submitted the form, android toast error message will be thrown and this form will not be opened.
     */
    onSubmitPress() {
        Alert.alert(
            'Warning',
            'Once feedback submitted, you cannot edit it again.\
            \n\nPress OK to select.',
            [
                {
                    text: 'OK', onPress: () => {
                        global.storage.save({
                            key: 'tCFSubmitted',
                            data: true
                        }).catch(e => {
                            console.log("Error: ", e);
                        });
                        global.storage.save({
                            key: 'tCFeedbackPushed',
                            data: false
                        });
                        Actions.selectFeedbackForm();
                    }
                },
                {
                    text: 'Cancel', onPress: () => {
                    }
                }
            ]
        );

    }

    componentWillMount() {
        global.storage.load({
            key: 'tCFInputsFirstPart',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            console.log("tCFInputsFirstPart: " + JSON.stringify(res));
            if(res) {
                this.setState({
                    inputs_first_part: res
                });  
            }          
        }).catch(e => {
            console.log("Error: ", e);            
        }); 
        global.storage.load({
            key: 'tCFInputsSecondPart',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            console.log("tCFInputsSecondPart: " + JSON.stringify(res));  
            if (res) {
                this.setState({
                    inputs_second_part: res
                });       
            }    
        }).catch(e => {
            console.log("Error: ", e);            
        });
        global.storage.load({
            key: 'tCFInputsEnroute',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            console.log("tCFInputsEnroute: " + JSON.stringify(res));   
            if (res) {
                this.setState({
                    inputs_enroute: res
                });       
            }   
        }).catch(e => {
            console.log("Error: ", e);            
        });
        global.storage.load({
            key: 'tCFInputsAbst',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            console.log("tCFInputsAbst: " + JSON.stringify(res));
            if (res) {
                this.setState({
                    inputs_abst: res
                }); 
            }
        }).catch(e => {
            console.log("Error: ", e);
        });
    }

    render() {
        var { height, width } = Dimensions.get('window');
        DEVICE_WIDTH = width;
        return (
            <View style={styles.container}>
                <KeyboardAwareScrollView style={styles.feedbackcontainer}>
                    {this.state.inputs_first_part.map((input, index) => {
                        return (
                            <View key={index}>
                                <Text style={styles.assignText}>
                                    {input.i}
                                </Text>
                                <TextInput
                                    style={styles.textinput}
                                    autoCapitalize={'none'}
                                    returnKeyType={'done'}
                                    placeholderTextColor='white'
                                    underlineColorAndroid='transparent'
                                    autoCorrect={true}
                                    value={this.state.inputs_first_part[index].text}
                                    onChangeText={(text)=>this.onInputFirstPartChanged(index,text)}/>
                            </View>
                        );
                    })}
                    <Text style={{ color: 'white' }}>
                        Enroute Boarding / Seat
                    </Text> 
                    <ScrollView style={{ flex: 1, flexDirection: "row" }}
                        horizontal={true}>                                               
                        {this.state.inputs_enroute.map((input, index) => {
                            return (
                                <View key={index}>
                                    <Text style={{ color: 'white', marginHorizontal: 20 }}>
                                        {input.i}                                  
                                    </Text>
                                    <TextInput
                                        style={styles.input}
                                        autoCapitalize={'none'}
                                        returnKeyType={'done'}
                                        autoCorrect={false} 
                                        keyboardType='numeric'
                                        value={this.state.inputs_enroute[index].text}
                                        onChangeText={(text) => this.onEnrouteEntered(index, text)}
                                        maxLength={4}
                                    />
                                </View>
                            );
                        })}
                    </ScrollView>
                    <Text style={{ color: 'white' }}>
                        Absentees
                        </Text>
                    <ScrollView style={{ flex: 1, flexDirection: "row" }}
                        horizontal={true}>                        
                        {this.state.inputs_abst.map((input, index) => {
                            return (
                                <View key={index}>
                                    <Text style={{ color: 'white', marginHorizontal: 20 }}>
                                        {input.i}
                                    </Text>
                                    <TextInput
                                        style={styles.input}
                                        autoCapitalize={'none'}
                                        returnKeyType={'done'}
                                        autoCorrect={false}
                                        keyboardType='numeric'
                                        value={this.state.inputs_abst[index].text}
                                        onChangeText={(text) => this.onAbstEntered(index, text)}
                                        maxLength={4}
                                    />
                                </View>
                            );
                        })}
                    </ScrollView>
                    {this.state.inputs_second_part.map((input, index) => {
                        return (
                            <View key={index}>
                                <Text style={styles.assignText}>
                                    {input.i}
                                </Text>
                                <TextInput
                                    style={styles.textinput}
                                    placeholderTextColor='white'
                                    underlineColorAndroid='transparent'
                                    autoCapitalize={'none'}
                                    returnKeyType={'done'}
                                    value={this.state.inputs_second_part[index].text}
                                    onChangeText={(text) => this.onInputSecondPartChanged(index, text)}                                    
                                    autoCorrect={true} />
                            </View>
                        );
                    })}                    
                </KeyboardAwareScrollView>
                <View style={styles.submitContainer} >
                    <TouchableHighlight style={styles.headerContainer} onPress={this.onSavePress.bind(this)}>
                        <Text style={styles.assignText}>
                            SAVE
                        </Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.headerContainer} onPress={this.onSubmitPress.bind(this)}>
                        <Text style={styles.assignText}>
                            SUBMIT
                        </Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
} 


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex: 1,
    },
    feedbackcontainer: {
        flex: 1
    },
    input: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        width: 40,
        height: 40,
        marginHorizontal: 20,
        color: '#ffffff',
        marginBottom: 10,
    },
    submitContainer: {
        flex: .10,
        flexDirection: 'row'
    },
    headerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4286f4',
        height: 40,
        padding: 20,
        top: 10,
        flex: .5,
        marginHorizontal: 5,
        marginBottom: 30,
        zIndex: 100,
    },
    assignText: {
        color: 'white',
        backgroundColor: 'transparent',
        fontSize: 16,
        textAlign: 'left',
        marginHorizontal: 20,      
    },
    textinput: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        width: DEVICE_WIDTH - 20,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 5,        
        color: '#ffffff',
        marginBottom: 10,
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(TourCoordinatorFeedback);