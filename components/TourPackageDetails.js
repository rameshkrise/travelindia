import React, {Component} from 'react';
import {View, ListView, Text, StyleSheet,
    TouchableHighlight, ToastAndroid, Alert, ScrollView} from 'react-native';
import NavigateFooter from './NavigateFooter';
import { Actions } from 'react-native-router-flux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import store from '../store/configureStore';

function mapStateToProps(state) {
    console.log(state);
    return {
        tourPackages: state.tourpackage.tourPackages,
        travellerDetails: state.tourpackage.travellerDetails,
        webServiceToken: state.login.webServiceToken,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

let loadingStatus;
const MARGIN = 40;

function select(state){
    return state.tourpackage.tourPackagesStatus;
}


function getTourPackageLoadingStatus(){
    loadingStatus = select(store.getState());
    console.log("loadingStatus", loadingStatus);
}

let unsubscribe = store.subscribe(getTourPackageLoadingStatus);

class TourPackageDetails extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds,
            selectedPackage:null,
            selectedPackageId: -1,
        };        
    }
    
    componentWillMount() {
        //Loading the tour package from permenent storage.
        global.storage.load({
            key: 'selectedPackage',
            autoSync: true,
            syncInBackground: true,
        }).then(ret => {
            console.log("Reterieved from storage: " + JSON.stringify(ret));            
            this.setState({
                selectedPackage: ret
            });
        }).catch(e => {
            console.log("Error: ", e);
        });
        if(this.props.tourPackages) {
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(this.props.tourPackages)
            });
        } else {
            ToastAndroid.showWithGravity('There was an error while fetching Tour Packages', ToastAndroid.LONG, ToastAndroid.CENTER);            
        }
        
    }

    onPackageSelection(rowData, rowId){
        Alert.alert(
            'Warning',
            'Upon selecting a package, previously stored user feedback and coordinator feedback data will be deleted\
            \n\nPress OK to select.',
            [
                {text: 'OK', onPress: () => {
                    this.setState({
                        selectedPackage: rowData.tour_title,
                        selectedPackageId: rowData.tour_id
                    });
                    this.props.fetchTravellersDetails(rowData.tour_id, this.props.webServiceToken);
                    ToastAndroid.showWithGravity('Selected Package is ' + JSON.stringify(rowData.tour_title), ToastAndroid.SHORT, ToastAndroid.CENTER);
                    //Store the selected package permenently, so that next time app can show what package is been selected currently.
                    global.storage.save({
                        key: 'selectedPackage',
                        data: this.state.selectedPackage
                    });
                    //Store the selected pakage Id permenently to push the traveller feedback data with package id.
                    global.storage.save({
                        key: 'selectedPackageId',
                        data: this.state.selectedPackageId
                    });
                    //TODO:: Need to call clear user & coordinator feedback data.
                    global.storage.save({
                        key: 'tCFSubmitted',
                        data: false
                    }).catch(e => {
                        console.log("Error: ", e);
                    });
                    global.storage.save({
                        key: 'userFeedbackPushed',
                        data: false
                    });
                    global.storage.save({
                        key: 'tCFeedbackPushed',
                        data: false
                    });
                    global.storage.remove({
                        key: 'userFeedback'
                    });
                    global.storage.remove({
                        key: 'tCFInputsFirstPart'
                    });
                    global.storage.remove({
                        key: 'tCFInputsSecondPart'
                    });
                    global.storage.remove({
                        key: 'tCFInputsEnroute'
                    });
                    global.storage.remove({
                        key: 'tCFInputsAbst'
                    });
                    global.storage.remove({
                        key: 'submittedTravellerIds'
                    });
                }},
                {text: 'Cancel', onPress: () => {

                }}
            ]
        );
       
    }

    onHomePress(){
        Actions.adminOptions();
    }

    render() {        
        return (
            <View style={styles.container}>
                <View>
                    <TouchableHighlight style={styles.headerContainer}>
                        <Text style={styles.assignText}>
                            Select the package to assign
                        </Text>
                    </TouchableHighlight>
                </View>
                <ScrollView>
                <ListView 
                    dataSource={this.state.dataSource}
                    renderRow={(rowData, sectionId, rowId) => {
                        return(
                            <TouchableHighlight onPress={this.onPackageSelection.bind(this, rowData, rowId)}>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.text}>{rowData.tour_title}</Text>
                                </View>
                            </TouchableHighlight>
                            );
                        }
                    }
                /> 
                </ScrollView>
                <View style={styles.packageContainer}> 
                    <Text style={styles.assignText}>
                        Selected Package - {this.state.selectedPackage}
                    </Text>  
                </View>
                <NavigateFooter onPress={this.onHomePress} navigationText='HOME'/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'black'
    },
    rowContainer: {
        padding: 10,
        margin: 3,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#827f76',
        alignItems: 'center',
        zIndex: 100
    },
    rowSelectedContainer : {
        padding: 10,
        marginTop: 3,
        backgroundColor: '#ebc60e',
        alignItems: 'center',        
    },
    packageContainer : {
        flex: 0.20,
    },
    headerContainer : {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4286f4',
        height: MARGIN,
        top: 10,
        marginBottom: 10
    },
    assignText: {
        color: 'white',
        backgroundColor: 'transparent',   
        fontSize: 16,
        textAlign: 'center'    
    },
    text: {
        alignItems: 'center',
        color: 'white',
        fontSize: 16,
        
    },
})


export default connect(mapStateToProps, mapDispatchToProps)(TourPackageDetails);

