import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import FeedbackForm from './FeedbackForm';

import arrowImg from '../images/left-arrow.png';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        //travellerDetails: state.tourpackage.travellerDetails ,
        selectedTraveller: state.tourpackage.selectedTraveller
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

const SIZE = 40;

class TravellerFeedback extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            isLoading: false,
        };

        this._onPress = this._onPress.bind(this);
        //this.growAnimated = new Animated.Value(0);
    }

    _onPress() {
        if (this.state.isLoading) return;
        this.setState({ isLoading: true });        
    }

    render() {
        // const changeScale = this.growAnimated.interpolate({
        //     inputRange: [0, 1],
        //     outputRange: [1, SIZE],
        // });
        console.log('TravellerFeedbcak.js ', JSON.stringify(this.props));
        console.log("selectedTravellerDetails", this.props.selectedTraveller);
        return (
            <View style={styles.container}>
                <FeedbackForm details={this.props.selectedTraveller}/>                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        width: SIZE,
        height: SIZE,
        borderRadius: 100,
        zIndex: 99,
        backgroundColor: '#F035E0',
    },
    circle: {
        height: SIZE,
        width: SIZE,
        marginTop: -SIZE,
        borderRadius: 100,
        backgroundColor: '#F035E0',
    },
    image: {
        width: 24,
        height: 24,
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(TravellerFeedback)