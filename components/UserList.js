import React, { Component, PropTypes } from 'react';

import {
    StyleSheet,
    View,
    ListView,
    Text,
    TouchableHighlight,
    ToastAndroid,    
} from 'react-native';

import NavigateFooter from './NavigateFooter';
import { Actions } from 'react-native-router-flux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

function mapStateToProps(state) {
    return {
        submittedTravellerIds: state.tourpackage.submittedTravellerIdCollection || []
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}

class UserList extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds,
            travellerDetails: null,
        };
    }

    onRowPress(rowData, rowId) {
        //Get the traveler data based on rowId from the state updated by the storage reterival.
        let selectedTravellerDetails = rowData;
        //Pass the details to next screen to populate them through redux store.
        this.props.selectedTravellerDetails(selectedTravellerDetails);

        Actions.travellerFeedback();
    }

    getNonSubmittedUserList(submittedTravellerIds) {
        global.storage.load({
            key: 'travellerDetails',
            autoSync: true,
            syncInBackground: true,
        }).then(res => {
            console.log("Reterieved from storage: " + JSON.stringify(res));
            let travllersDispalyNames = res.map(r => {
                return r.travelers_name + " " + r.travelers_mobile_1;
            });
            let nonSubmittedUserList = res.filter(r => {
                if (submittedTravellerIds.indexOf(r.travelers_id) < 0) {
                    return r;
                }
            });
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(nonSubmittedUserList),
                travellerDetails: nonSubmittedUserList
            });
        });
    }

    componentWillMount() {
        global.storage.load({
            key: 'submittedTravellerIds',
            autoSync: true,
            syncInBackground: true,
        }).then(submittedTravellerIds => {
            this.getNonSubmittedUserList(submittedTravellerIds);
        }).catch(e => {
            this.getNonSubmittedUserList([]);            
        });     
    }

    onHomeButtonPress() {
        Actions.selectFeedbackForm();
    }

    render() {
        return (
            <View style={{ backgroundColor: 'black', flex: 1 }} >
                <ListView style={{ flex: 1 }}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData, sectionId, rowId) => {
                        return (
                            <TouchableHighlight onPress={this.onRowPress.bind(this, rowData, rowId)}>
                                <View style={styles.container}>
                                    <Text style={styles.text}>{rowData.travelers_name + " " + rowData.travelers_mobile_1}</Text>
                                </View>
                            </TouchableHighlight>
                        )
                    }}
                    enableEmptySections={true}
                />                
                <NavigateFooter onPress={this.onHomeButtonPress} navigationText='HOME'/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        margin: 10,
        marginLeft: 20,
        marginRight:20,
        backgroundColor: '#4286f4',
        alignItems: 'center',
    },
    text: {
        color: 'white',
        fontSize: 16,
        textAlign: 'center',
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserList);