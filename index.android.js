/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  
} from 'react-native';

import RootComponent from './components/Root';


AppRegistry.registerComponent('AwesomeProject', () => RootComponent);
