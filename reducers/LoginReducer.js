export default function login(state = [], action) {
    switch(action.type) {
        //Sets either true/
        case 'LOGIN':
            return {...state, loggedIn: action.loggedIn};        
        case 'AGENT_NAME':  
            return {...state, agentName: action.agentName};  
        case 'AGENT_PASSWORD':
            return {...state, agentPassword: action.agentPassword};
        case 'CHANGE_CREDENTIALS':
            return {...state, credentialType: action.credentialType};   
        case 'WEBSERVICE_TOKEN':
            return {...state, webServiceToken: action.webServiceToken};     
        default: 
            return state;
    }
}