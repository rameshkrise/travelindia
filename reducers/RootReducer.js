import { combineReducers } from 'redux';
import  login from './LoginReducer';
import tourpackage from './TourPackageReducer';

const rootReducer =  combineReducers({
    login,
    tourpackage
});

export default rootReducer;

