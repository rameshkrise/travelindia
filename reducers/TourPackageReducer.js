
export default function tourpackage(state = [], action) {
    switch(action.type){
        case "RECEIVE_PACKAGES":
            return {...state, tourPackages: action.packages};
        case "RECEIVE_PACKAGES_STATUS":
            return {...state, tourPackagesStatus: action.status};
        case "TRAVELLER_DETAILS":
            global.storage.save({
                key: 'travellerDetails',
                data: action.travellers
            });
            console.log("Reducer: Traveller Details: "+ JSON.stringify(action.travellers));
            return { ...state, travellerDetails: action.travellers};
        case "SELECTED_TRAVELLER_DETAILS":
            return {...state, selectedTraveller: action.details};
        case "SUBMITTED_TRAVELLER_ID_COLLECTION":
            return {...state, submittedTravellerIdCollection: action.ids}
        case "TRAVELLER_FEEDBACK":
            return {...state, travellerFeedback: action.feedback}
        default:
            return state;
    }
}

