import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import {createLogger} from 'redux-logger';

import rootReducer from '../reducers/RootReducer';

const logger = createLogger();
const enhancer = compose(
    applyMiddleware(thunk, promise, logger)
);
const initialState = undefined;

const store =   createStore(rootReducer, initialState, enhancer);
export default store;