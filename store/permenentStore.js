import Realm from 'realm';

class TravellerDetail {
    static get() { return realm.objects(TravellerDetail.schema.name) }
    static schema = {
        name: 'TravellerDetail',
        primaryKey: 'id',
        properties: {
            id: { type: 'string' },
            travellerName: { type: 'string' },
            travellerMailId: {type: 'string'},
            completedFeedback: { type: 'bool', default: false },
            createdTimestamp: { type: 'date' }
        }
    }
}

// Create Realm DB
const realm = new Realm({ schema: [TravellerDetail] });

export const getTravellerDetail = (id) => {
    const travellerDetail = realm.objectForPrimaryKey(TravellerDetail, id);
    return travellerDetail;
}

//Create new traveller details
export const createTravellerDetail = (detailsObj) => {
    realm.write(() => {
        realm.create(TravellerDetail.schema.name, {
            id: detailsObj.id,
            details: detailsObj,
        })
    })
}

export const deleteTravellerDetail = (travellerDetail) => {
    realm.write(() => {
        realm.delete(travellerDetail);
    })
}

export const updateTravellerFeedback = (id, travellerFeedback) => {
    realm.write(() => {
        try{
            //TODO:: Fetch the particular traveller detail based on the id and update.
            travellerDetail.feedback = travellerFeedback;
        } catch(e){
            console.warn(e);
        }
    })
}